@extends('layouts.app')


@section('content')

  <x-Banner-section></x-Banner-section>

  <x-Task--app></x-Task--app>
  <!-- Page Wraper -->
  <div class="page_wrapper">

    <x-about-us></x-about-us>

    <x-how-it></x-how-it>

    <x-text-list-flow></x-text-list-flow>

    {{-- <x-service-section></x-service-section> --}}


  </div>
  <!-- Wraper End -->

  <x-reviews></x-reviews>

  <!-- Page Wraper -->
  <div class="page_wrapper">

    {{-- <x-clients></x-clients> --}}

    {{-- <x-pricing></x-pricing> --}}

    <x-app-interface></x-app-interface>

    {{-- <x-blog></x-blog> --}}

    <x-contact-form></x-contact-form>


@endsection
