<!-- Task-App-Section-Start -->
<section class="row_am task_app_section">
    <!-- Task Block start -->
    <div class="task_block">
      <div class="dotes_blue"><img src="images/blue_dotes.png" alt="image"></div>
      <!-- row start -->
      <div class="row">
        <div class="col-md-6">
          <!-- task images -->
          <div class="task_img" data-aos="fade-in" data-aos-duration="1500">
            <div class="frame_img">
              <img src="images/feature1a.png" alt="image">
            </div>
            <div class="screen_img">
              <img class="moving_animation" src="images/feature1b.png" alt="image">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <!-- task text -->
          <div class="task_text">
            <div class="section_title white_text" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="100">
              <span class="title_badge">Unique Features</span>
              <span class="icon">
                <img src="images/feature-icon1.png" alt="image">
              </span>
              <!-- h2 -->
              <h2>Task creation and manage
                lorem ipsum dollar</h2>
              <!-- p -->
              <p>
                Lorem Ipsum is simply dummy text of the printing and
                setting indus orem Ipsum has been the industrys standard
                dummxt ever since the when an own printer
                took a galley of type and scrambled it to make.
                a type specimen book.
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- row end -->
    </div>
    <!-- Task Block end -->
    <!-- Task Block start -->
    <div class="task_block">
      <div class="dotes_blue"><img src="images/blue_dotes.png" alt="image"></div>
      <!-- row start -->
      <div class="row">
        <div class="col-md-6">
          <!-- task images -->
          <div class="task_img" data-aos="fade-in" data-aos-duration="1500">
            <div class="frame_img">
              <img src="images/feature2a.png" alt="image">
            </div>
            <div class="screen_img">
              <img class="moving_animation" src="images/feature2b.png" alt="image">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <!-- task text -->
          <div class="task_text">
            <div class="section_title white_text" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="100">
              <span class="title_badge">Unique Features</span>
              <span class="icon">
                <img src="images/feature-icon2.png" alt="image">
              </span>
              <!-- h2 -->
              <h2>Client comunication for best results</h2>
              <!-- p -->
              <p>
                Lorem Ipsum is simply dummy text of the printing and
                setting indus orem Ipsum has been the industrys standard
                dummxt ever since the when an own printer
                took a galley of type and scrambled it to make.
                a type specimen book.
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- row end -->
    </div>
    <!-- Task Block end -->
    <!-- Task Block start -->
    <div class="task_block">
      <div class="dotes_blue"><img src="images/blue_dotes.png" alt="image"></div>
      <!-- row start -->
      <div class="row">
        <div class="col-md-6">
          <!-- task images -->
          <div class="task_img" data-aos="fade-in" data-aos-duration="1500">
            <div class="frame_img">
              <img src="images/feature3a.png" alt="image">
            </div>
            <div class="screen_img">
              <img class="moving_animation" src="images/feature3b.png" alt="image">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <!-- task text -->
          <div class="task_text">
            <div class="section_title white_text" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="100">
              <span class="title_badge">Unique Features</span>
              <span class="icon">
                <img src="images/feature-icon3.png" alt="image">
              </span>
              <!-- h2 -->
              <h2>Live chat with Video Call</h2>
              <!-- p -->
              <p>
                Lorem Ipsum is simply dummy text of the printing and
                setting indus orem Ipsum has been the industrys standard
                dummxt ever since the when an own printer
                took a galley of type and scrambled it to make.
                a type specimen book.
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- row end -->
    </div>
    <!-- Task Block end -->
  </section>
  <!-- Task-App-Section-end -->
