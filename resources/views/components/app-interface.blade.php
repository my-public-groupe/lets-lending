<!-- Beautifull-interface-Section start -->
<section class="row_am interface_section">
    <!-- container start -->
    <div class="container-fluid">
      <div class="section_title" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="300">
        <span class="title_badge">App screens</span>
        <h2>Userfriendly <span>interface</span> design</h2>
      </div>

      <!-- screen slider start -->
      <div class="screen_slider" data-aos="fade-up" data-aos-duration="1500">
        <div id="screen_slider" class="owl-carousel owl-theme">
          <div class="item">
            <div class="screen_frame_img">
              <img src="images/intrscrn1.png" alt="image">
            </div>
          </div>
          <div class="item">
            <div class="screen_frame_img">
              <img src="images/intrscrn2.png" alt="image">
            </div>
          </div>
          <div class="item">
            <div class="screen_frame_img">
              <img src="images/intrscrn3.png" alt="image">
            </div>
          </div>
          <div class="item">
            <div class="screen_frame_img">
              <img src="images/intrscrn4.png" alt="image">
            </div>
          </div>
          <div class="item">
            <div class="screen_frame_img">
              <img src="images/intrscrn5.png" alt="image">
            </div>
          </div>
          <div class="item">
            <div class="screen_frame_img">
              <img src="images/intrscrn2.png" alt="image">
            </div>
          </div>
        </div>
      </div>
      <!-- screen slider end -->
    </div>
    <!-- container end -->
  </section>
  <!-- Beautifull-interface-Section end -->
<!-- Text List flow Section Start -->
<div class="text_list_section row_am downaload_section" data-aos="fade-in" data-aos-duration="1500">
    <div class="container">
      <div class="yellow_dotes">
        <img src="images/yellow_dotes.png" alt="image">
      </div>
      <div class="center_screen">
        <div class="img">
          <img src="images/downloadScreen.png" alt="image">
        </div>
        <!-- app buttons -->
        <ul class="app_btn">
          <li>
            <a href="#">
              <img class="blue_img" src="images/googleplay.png" alt="image">
            </a>
          </li>
          <li>
            <a href="#">
              <img class="blue_img" src="images/appstorebtn.png" alt="image">
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="background_slider">
      <div class="dowanload_slider">
        <div class="downlist">
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
        </div>
      </div>
      <div class="slider_block">
        <div class="owl-carousel owl-theme" id="text_list_flow_download">
          <div class="item">
            <div class="text_block">
              <span>Download </span>
              <span class="mark_star">•</span>
            </div>
          </div>
          <div class="item">
            <div class="text_block">
              <span>Download </span>
              <span class="mark_star">•</span>
            </div>
          </div>
          <div class="item">
            <div class="text_block">
              <span>Download </span>
              <span class="mark_star">•</span>
            </div>
          </div>
          <div class="item">
            <div class="text_block">
              <span>Download </span>
              <span class="mark_star">•</span>
            </div>
          </div>
          <div class="item">
            <div class="text_block">
              <span>Download </span>
              <span class="mark_star">•</span>
            </div>
          </div>
          <div class="item">
            <div class="text_block">
              <span>Download </span>
              <span class="mark_star">•</span>
            </div>
          </div>
        </div>
      </div>
      <div class="dowanload_slider">
        <div class="downlist">
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
          <div class="text_block">
            <span>Download </span>
            <span class="mark_star">•</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Text List flow Section End -->
