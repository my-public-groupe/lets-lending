<!-- Contact Form Section Start -->
<section class="contact_form white_text row_am" data-aos="fade-in" data-aos-duration="1500">
    <div class="contact_inner">
      <div class="container">
        <div class="dotes_blue"><img src="images/blue_dotes.png" alt="image"></div>
        <div class="section_title" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="100">
          <span class="title_badge">Message us</span>
          <h2>Drop a message us</h2>
          <p>Fill up form below, our team will get back soon</p>
        </div>
        <form data-aos="fade-up" data-aos-duration="1500">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Name *" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Email *" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Company Name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select class="form-control">
                  <option value="">Country</option>
                  <option value="">India</option>
                  <option value="">USA</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Phone">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Website">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <textarea class="form-control" placeholder="Comments"></textarea>
              </div>
            </div>
            <div class="col-md-8">
              <div class="coustome_checkbox">
                <label for="term_checkbox">
                  <input type="checkbox" id="term_checkbox">
                  <span class="checkmark"></span>
                  I agree to receive emails, newsletters and promotional messages
                </label>
              </div>
            </div>
            <div class="col-md-4 text-right">
              <div class="btn_block">
                <button class="btn puprple_btn ml-0">Submit</button>
                <div class="btn_bottom"></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
  <!-- Contact Form Section End -->
