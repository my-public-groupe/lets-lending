<!-- Positive Reviews Section Start -->
<section class="review_section row_am">
    <div class="container">
      <div class="positive_inner">
        <div class="row">
          <div class="col-md-6 sticky-top">
            <div class="sidebar_text" data-aos="fade-up" data-aos-duration="1500">
              <div class="section_title text-left">
                <span class="title_badge">Reviews</span>
                <h2><span>Positive reviews </span> <br>
                  of our clients</h2>
              </div>
              <div class="google_rating">
                <div class="star">
                  <span><i class="icofont-star"></i></span>
                  <span><i class="icofont-star"></i></span>
                  <span><i class="icofont-star"></i></span>
                  <span><i class="icofont-star"></i></span>
                  <span><i class="icofont-star"></i></span>
                </div>
                <p>4.5/5.0 Rated on <img class="img-fluid" src="images/google.png" alt="image"></p>
              </div>
              <div class="user_review">
                <p>1399 <a href="#">Total user reviews <i class="icofont-arrow-right"></i></a></p>
              </div>
              <div class="smiley_icon"><img src="images/smily.png" alt="image"></div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="review_side">
              <div class="review_block" data-aos="fade-up" data-aos-duration="1500">
                <div class="coustomer_info">
                  <div class="avtar">
                    <img src="images/review1.png" alt="image">
                    <div class="text">
                      <h3>Willium Joy</h3>
                      <span>Smartbrain Tech</span>
                    </div>
                  </div>
                  <div class="star">
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Sapiente culpa, dolores ullam
                  laudantium deleniti ipsa qui saepe voluptatum nam pariatur?
                  Lorem ipsum dolor sit amet
                  consectetur adipisicing elit. Neque, totam.</p>
              </div>
              <div class="review_block" data-aos="fade-up" data-aos-duration="1500">
                <div class="coustomer_info">
                  <div class="avtar">
                    <img src="images/review2..png" alt="image">
                    <div class="text">
                      <h3>John Due</h3>
                      <span>Corporate Agency</span>
                    </div>
                  </div>
                  <div class="star">
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Sapiente culpa, dolores ullam
                  laudantium deleniti ipsa qui saepe voluptatum nam pariatur?
                  Lorem ipsum dolor sit amet
                  consectetur adipisicing elit. Pariatur et, nemo distinctio
                  eum
                  omnis quam!</p>
              </div>
              <div class="review_block" data-aos="fade-up" data-aos-duration="1500">
                <div class="coustomer_info">
                  <div class="avtar">
                    <img src="images/review3..png" alt="image">
                    <div class="text">
                      <h3>Maria</h3>
                      <span>Company Inc</span>
                    </div>
                  </div>
                  <div class="star">
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                    <span><i class="icofont-star"></i></span>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Sapiente culpa, dolores ullam
                  laudantium deleniti ipsa qui saepe voluptatum nam pariatur?
                  Lorem ipsum dolor sit amet
                  consectetur adipisicing elit. Pariatur et, nemo distinctio
                  eum
                  omnis quam!</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Positive Reviews Section End -->
