<!-- Footer-Section start -->
<footer class="white_text" data-aos="fade-in" data-aos-duration="1500">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="logo_side">
            <div class="logo">
              <a href="#">
                <img src="images/ft_logo.png" alt="Logo">
              </a>
            </div>
            <div class="news_letter">
              <h3>Subscribe newsletter</h3>
              <p>Be the first to recieve all latest post in your inbox</p>
              <form>
                <div class="form-group">
                  <input type="email" class="form-control" placeholder="Enter your email">
                  <button class="btn"><i class="icofont-paper-plane"></i></button>
                </div>
                <p class="note">By clicking send link you agree to receive message.</p>
              </form>
            </div>
            <ul class="contact_info">
              <li><a href="mailto:">support@example.com</a></li>
              <li><a href="tel: ">+1-900-123 4567</a></li>
            </ul>
            <ul class="social_media">
              <li><a href="#"><i class="icofont-facebook"></i></a></li>
              <li><a href="#"><i class="icofont-twitter"></i></a></li>
              <li><a href="#"><i class="icofont-instagram"></i></a></li>
              <li><a href="#"><i class="icofont-pinterest"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-6">
          <div class="download_side">
            <h3>Download app</h3>
            <ul class="app_btn">
              <li>
                <a href="#">
                  <img class="blue_img" src="images/googleplay.png" alt="image">
                </a>
              </li>
              <li>
                <a href="#">
                  <img class="blue_img" src="images/appstorebtn.png" alt="image">
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer_bottom">
      <div class="container">
        <div class="ft_inner">
          <div class="copy_text">
            <p>© Copyrights 2023. All rights reserved.</p>
          </div>
          <ul class="links">
            <li><a href="index.html">Home</a></li>
            <li><a href="about.html">About us</a></li>
            <li><a href="pricing.html">Pricing</a></li>
            <li><a href="blog-list.html">Blog</a></li>
            <li><a href="contact.html">Contact us</a></li>
          </ul>
          <div class="design_by">
            <p>Crafted by <a href="https://themeforest.net/user/kalanidhithemes" target="blank">Kalanidhi Themes</a></p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Footer-Section end -->
