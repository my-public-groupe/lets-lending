<!-- Blog Section Start -->
<section class="blog_section row_am">
    <div class="container">
      <div class="section_title" data-aos="fade-up" data-aos-duration="1500">
        <span class="title_badge">Lates updates</span>
        <h2>Our latest <span>blog post</span></h2>
      </div>
      <div class="blog_listing">
        <div class="blog_post" data-aos="fade-up" data-aos-duration="1500">
          <a href="#" class="img">
            <img src="images/blog1.png" alt="image">
          </a>
          <div class="text">
            <ul class="blog_info">
              <li>May 12, 2023</li>
              <li>Mobile app</li>
              <li>5 Comments</li>
            </ul>
            <h3><a href="#">Effective ways to monetize mobile
                apps for better perfomance</a></h3>
            <div class="tag_more">
              <span class="tag">Mobile app</span>
              <a href="#">Read more <i class="icofont-arrow-right"></i></a>
            </div>
          </div>
        </div>
        <div class="blog_post" data-aos="fade-up" data-aos-duration="1500">
          <a href="#" class="img">
            <img src="images/blog2.png" alt="image">
          </a>
          <div class="text">
            <ul class="blog_info">
              <li>May 12, 2023</li>
              <li>Mobile app</li>
              <li>5 Comments</li>
            </ul>
            <h3><a href="#">Why you our app, top 5 reason to
                choose our app</a></h3>
            <div class="tag_more">
              <span class="tag">User experience</span>
              <a href="#">Read more <i class="icofont-arrow-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Blog Section End -->
