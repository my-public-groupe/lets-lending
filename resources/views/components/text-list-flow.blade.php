<!-- Text List flow Section Start -->
<div class="text_list_section row_am" data-aos="fade-in" data-aos-duration="1500">
    <div class="container">
      <span class="title_badge down_fix">Why choose our app</span>
    </div>
    <div class="slider_block">
      <div class="owl-carousel owl-theme" id="text_list_flow">
        <div class="item">
          <div class="text_block">
            <span>Multiple campaigns </span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span>User friendly</span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span>Advanced analytics </span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span> Task management</span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span> Event Scheduler</span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span> Group Video Calls</span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span>Live Chat</span>
            <span class="mark_star">•</span>
          </div>
        </div>
        <div class="item">
          <div class="text_block">
            <span>Activity Streem</span>
            <span class="mark_star">•</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Text List flow Section End -->
