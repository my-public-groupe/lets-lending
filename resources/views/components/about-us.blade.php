<!-- About Us Start-->
<section class="about_section row_am">
    <div class="container">
      <div class="section_title" data-aos="fade-up" data-aos-duration="1500">
        <span class="title_badge mb-1">About us</span>
        <h2>Application with the best <img src="images/mobileicon.png" alt="image"> user <br>
          interface convert <span><img src="images/usericon.png" alt="image"> visitor</span> into <span><img
              src="images/magnet.png" alt="image"> leads</span></h2>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6">
          <ul class="app_statstic" id="counter" data-aos="fade-in" data-aos-duration="1500">
            <li data-aos="fade-up" data-aos-duration="1500">
              <div class="text">
                <p><span class="counter-value" data-count="17">0</span><span>M+</span></p>
                <p>Download</p>
              </div>
            </li>
            <li data-aos="fade-up" data-aos-duration="1500">
              <div class="text">
                <p><span class="counter-value" data-count="2300">1500</span><span>+</span></p>
                <p>Reviews</p>
              </div>
            </li>
            <li data-aos="fade-up" data-aos-duration="1500">
              <div class="text">
                <p><span class="counter-value" data-count="150">0</span><span>+</span></p>
                <p>Countries</p>
              </div>
            </li>
            <li data-aos="fade-up" data-aos-duration="1500">
              <div class="text">
                <p><span class="counter-value" data-count="08">0 </span><span>M+</span></p>
                <p>Followers</p>
              </div>
            </li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="abtImg text-center" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/appscreen.png" alt="image">
          </div>
        </div>
        <div class="col-lg-4">
          <p data-aos="fade-up" data-aos-duration="1500">Lorem Ipsum is simply dummy text of the printing and
            typesetting industry lorem Ipsum has been the
            industrys standard dummy text ever since the when an unknown
            printer took a galley of type and scrambled
            it to make a type specimen book.</p>
          <div class="video_block" data-aos="fade-up" data-aos-duration="1500">
            <img class="thumbnil" src="images/applicationvideothumb.png" alt="image">
            <div class="playBtn">
              <a href="#" class="popup-youtube play-button play_icon"
                data-url="https://www.youtube.com/embed/tgbNymZ7vqY?autoplay=1&mute=1" data-toggle="modal" data-target="#myModal" title="XJj2PbenIsU"><img src="images/play_white.svg" alt="img"></a>
              <img class="spin_text" src="images/playvideotext.png" alt="image">
            </div>
          </div>
          <div class="btn_block" data-aos="fade-up" data-aos-duration="1500">
            <a href="#" class="btn puprple_btn ml-0">START FREE TRIAL</a>
            <div class="btn_bottom"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- About Us End -->
