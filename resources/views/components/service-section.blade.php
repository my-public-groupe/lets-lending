<!-- Service Section Start -->
<section class="row_am service_section">
    <div class="container">
      <div class="section_title" data-aos="fade-up" data-aos-duration="1500">
        <span class="title_badge mb-1">Services</span>
        <h2>Premium <span>services</span> of <br>
          our application</h2>
      </div>
      <div class="row service_blocks">
        <div class="col-md-6">
          <div class="service_text" data-aos="fade-up" data-aos-duration="1500">
            <div class="service_badge"><i class="icofont-tasks-alt"></i>
              <span>Task Manage</span>
            </div>
            <h2><span>Task creation</span> & manage <br>
              lorem ipsum dollar</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typtting
              industry lorem Ipsum has been the
              industrys standard dummy text ever since.</p>
            <ul class="listing_block">
              <li>
                <div class="icon">
                  <span><i class="icofont-ui-check"></i></span>
                </div>
                <div class="text">
                  <h3>Automate all task</h3>
                  <p>Lorem Ipsum is simply dummy text of the printing and
                    setting industry.</p>
                </div>
              </li>
              <li>
                <div class="icon">
                  <span><i class="icofont-ui-check"></i></span>
                </div>
                <div class="text">
                  <h3>Get notify lorem</h3>
                  <p>Dummy text of the printing and typesetting industry
                    lorem Ipsum has been the.</p>
                </div>
              </li>
            </ul>
            <div class="btn_block">
              <a href="#" class="btn puprple_btn ml-0">Start Free Trial</a>
              <div class="btn_bottom"></div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="img" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/service1.png" alt="image">
          </div>
        </div>
      </div>
      <div class="row service_blocks flex-row-reverse">
        <div class="col-md-6">
          <div class="service_text right_side" data-aos="fade-up" data-aos-duration="1500">
            <div class="service_badge"><i class="icofont-ui-clock"></i>
              <span>Schedule Meeting</span>
            </div>
            <h2><span>Manage project</span> and
              track lorem</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and
              typesetting industry lorem Ipsum has been the
              industrys standard dummy.</p>
            <ul class="feature_list">
              <li>
                <div class="icon">
                  <span><i class="icofont-check-circled"></i></span>
                </div>
                <div class="text">
                  <p>Lorem Ipsum is simply dummy text</p>
                </div>
              </li>
              <li>
                <div class="icon">
                  <span><i class="icofont-check-circled"></i></span>
                </div>
                <div class="text">
                  <p>The printing and typesetting industry lorem</p>
                </div>
              </li>
              <li>
                <div class="icon">
                  <span><i class="icofont-check-circled"></i></span>
                </div>
                <div class="text">
                  <p>Has been the industrys dummy</p>
                </div>
              </li>
            </ul>
            <div class="btn_block">
              <a href="#" class="btn puprple_btn ml-0">Start Free Trial</a>
              <div class="btn_bottom"></div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="img" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/service2.png" alt="image">
          </div>
        </div>
      </div>
      <div class="row service_blocks">
        <div class="col-md-6">
          <div class="service_text" data-aos="fade-up" data-aos-duration="1500">
            <div class="service_badge"><i class="icofont-list"></i> <span>History</span></div>
            <h2><span>Task creation</span> & manage
              lorem ipsum dollar</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typtting
              industry lorem Ipsum has been the
              industrys standard dummy text ever since.</p>
            <ul class="feature_list">
              <li>
                <div class="icon">
                  <span><i class="icofont-check-circled"></i></span>
                </div>
                <div class="text">
                  <p>Lorem Ipsum is simply dummy text</p>
                </div>
              </li>
              <li>
                <div class="icon">
                  <span><i class="icofont-check-circled"></i></span>
                </div>
                <div class="text">
                  <p>The printing and typesetting industry lorem</p>
                </div>
              </li>
              <li>
                <div class="icon">
                  <span><i class="icofont-check-circled"></i></span>
                </div>
                <div class="text">
                  <p>Has been the industrys dummy</p>
                </div>
              </li>
            </ul>
            <div class="btn_block">
              <a href="#" class="btn puprple_btn ml-0">Start Free Trial</a>
              <div class="btn_bottom"></div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="img" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/service3.png" alt="image">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Service Section End -->
