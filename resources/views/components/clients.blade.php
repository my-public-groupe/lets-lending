<!-- Our Client -->
<section class="row_am our_client">
    <div class="container">
      <div class="section_title" data-aos="fade-up" data-aos-duration="1500">
        <span class="title_badge">Our clients</span>
        <h2>
          <span class="d-block">3500+ companies</span>
          using our applicaion
        </h2>
      </div>
      <!-- Our Client List -->
      <ul class="client_list">
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/paypal.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/spoty.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/shopboat.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/slack.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/envato.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/jquery.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/woocommerce.png" alt="image">
          </div>
        </li>
        <li>
          <div class="client_logo" data-aos="fade-up" data-aos-duration="1500">
            <img src="images/themeforest.png" alt="image">
          </div>
        </li>
      </ul>
    </div>
  </section>
